use crate::find::random_sleep;
use crate::find_matching_serial_port;
use midir::SendError;
use rhai::Engine;
use rtrb::RingBuffer;
use serialport::{Error, SerialPort};
use std::io::Read;
use std::time::Duration;
use std::{thread, thread::sleep};
use tracing::{debug, error, trace};

/// Messages that can get send or received from the channels
/// returned by find_and_start_serial_midi_channel()
pub enum Message {
    ReceivedMidi([u8; 3]),
    SendMidi([u8; 3]),
}

/// This method tries to find a matching serial port and
/// holds open a connection to this one. One can send and receive
/// midi messages from and to this serial port through the returned
/// channels.
/// In case of an error on the serial port, it will try to reconnect
/// to the serial port, the returned channels will stay alive.
pub fn start_serial_midi_finder(
    match_expression: String,
    baud_rate: u32,
    latency: Duration,
    timeout: Duration,
) -> (rtrb::Producer<Message>, rtrb::Consumer<Message>) {
    let (mut messages_out_tx, messages_out_rx) = RingBuffer::new(1024);
    let (messages_in_tx, mut messages_in_rx) = RingBuffer::new(1024);
    thread::spawn(move || {
        let mut engine = Engine::new();
        'outer: loop {
            let port =
                find_matching_serial_port(&mut engine, &match_expression, timeout, baud_rate);

            let (mut port, port_info) = if let Ok(Some((port, port_info))) = port {
                (port, port_info)
            } else {
                println!("Could not find any matching device :/");
                // Nothing found. Let's wait for some random seconds and then try again.
                // We will wait for a random time to reduce the risk of multiple parallel
                // running serial2midi processes blocking each other by simultanously accessing
                // the same device.
                random_sleep();
                continue 'outer;
            };

            println!("Connecting to serial port {}", port_info.port_name);

            loop {
                // This variable indicates if we shall sleep at the end of this iteration
                let mut should_sleep = true;

                // First see if we shall send anything to the serial port
                while let Ok(Message::SendMidi(three_bytes)) = messages_in_rx.pop() {
                    if let Err(e) = port.write_all(&three_bytes) {
                        error!("{} while trying to write to serial port", e);
                        continue 'outer;
                    }
                    // We wrote something to the midi port, maybe there's more work
                    // for us so let's not go to sleep after this iteration.
                    should_sleep = false;
                }

                // Next, read all midi messages that we received on the serial port
                'bytereadloop: loop {
                    match port.bytes_to_read() {
                        Ok(bytes_to_read) => {
                            if bytes_to_read < 3 {
                                // Not enough to read, let's break
                                break 'bytereadloop;
                            }
                            let first_byte = match read_one_byte(&mut port) {
                                Ok(first_byte)
                                    if first_byte & 0b10000000 > 0 && first_byte >= 0x80 =>
                                {
                                    first_byte
                                }
                                Ok(first_byte) => {
                                    error!("Skipping invalid first byte={first_byte:X?}");
                                    continue 'bytereadloop;
                                }
                                Err(err) => {
                                    error!(
                                        "Error while trying to read from serial port, {:?}",
                                        err
                                    );
                                    continue 'outer;
                                }
                            };
                            trace!("Found valid first byte={first_byte:X}");
                            let second_byte = match read_one_byte(&mut port) {
                                Ok(second_byte) if second_byte < 0x80 => second_byte,
                                Ok(second_byte) => {
                                    error!("Skipping invalid second byte={second_byte:X?}");
                                    continue 'bytereadloop;
                                }
                                Err(err) => {
                                    error!(
                                        "Error while trying to read from serial port, {:?}",
                                        err
                                    );
                                    continue 'outer;
                                }
                            };
                            trace!("Found valid second byte={second_byte:X}");
                            let third_byte = match read_one_byte(&mut port) {
                                Ok(third_byte) if third_byte < 0x80 => third_byte,
                                Ok(third_byte) => {
                                    error!("Skipping invalid third_byte={third_byte:X?}");
                                    continue 'bytereadloop;
                                }
                                Err(err) => {
                                    error!(
                                        "Error while trying to read from serial port, {:?}",
                                        err
                                    );
                                    continue 'outer;
                                }
                            };
                            trace!("Found valid third byte={third_byte:X}");

                            // We successfully received a midi message, let's push it to the channel
                            // so that one can read it from the outside of this thread
                            if let Err(e) = messages_out_tx.push(Message::ReceivedMidi([
                                first_byte,
                                second_byte,
                                third_byte,
                            ])) {
                                error!("{} while trying to send on to messages_out_tx channel", e);
                                continue 'outer;
                            }
                            // We successfully read something from this port, let's not go to sleep
                            // after this iteration, there might be more work.
                            should_sleep = false;
                        }
                        Err(err) => {
                            error!("{} while trying to look how many bytes we can read", err);
                            continue 'outer;
                        }
                    }
                }

                // If we didn't send or receive any midi messages, lets sleep to not busy loop
                if should_sleep {
                    sleep(latency);
                }
            }
        }
    });
    (messages_in_tx, messages_out_rx)
}

/// Read exactly one byte from the serial port. This method blocks until one
/// byte could be read or we time out (timeout is set on serial port).
pub fn read_one_byte(serial_port: &mut Box<dyn SerialPort>) -> Result<u8, Error> {
    let mut one_byte: [u8; 1] = [0];
    serial_port.read_exact(&mut one_byte)?;
    Ok(one_byte[0])
}
