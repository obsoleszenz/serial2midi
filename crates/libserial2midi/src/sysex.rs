use core::time;
use std::time::Duration;

use serialport::Error;
use serialport::SerialPort;

use tracing::debug;
use tracing::trace;
use tracing::warn;

use crate::serial::read_one_byte;

pub const MIDI_SYSEX: u8 = 0xF0;
pub const MIDI_SYSEX_TYPE_NON_REALTIME: u8 = 0x7E;
pub const MIDI_SYSEX_END: u8 = 0xF7;
pub const MIDI_SYSEX_GENERAL_INFORMATION: u8 = 0x06;
pub const MIDI_SYSEX_REQUEST_IDENTITY: u8 = 0x01;
pub const MIDI_SYSEX_REPLY_IDENTITY: u8 = 0x02;

#[derive(Debug)]
pub struct SysexIdentity {
    pub manufacturer: i64,
    pub family: i64,
    pub model: i64,
    pub version: String,
}

impl Default for SysexIdentity {
    fn default() -> Self {
        Self {
            manufacturer: -1,
            family: -1,
            model: -1,
            version: "".to_string(),
        }
    }
}

pub fn try_parsing_sysex_identity_reply(
    serial_port: &mut Box<dyn SerialPort>,
) -> Result<Option<SysexIdentity>, Error> {
    let sysex_byte = read_one_byte(serial_port)?;

    if sysex_byte != MIDI_SYSEX {
        trace!(
            "Received {:x}, but waiting for MIDI_SYSEX ({:X})",
            sysex_byte,
            MIDI_SYSEX
        );
        return Ok(None);
    }

    trace!("Received MIDI_SYSEX ({MIDI_SYSEX:X})");

    let sysex_nonrealtime_type = read_one_byte(serial_port)?;
    if sysex_nonrealtime_type != MIDI_SYSEX_TYPE_NON_REALTIME {
        trace!(
            "Received {:x}, but expected MIDI_SYSEX_TYPE_NON_REALTIME ({:X})",
            sysex_nonrealtime_type,
            MIDI_SYSEX_TYPE_NON_REALTIME
        );
        return Ok(None);
    }

    trace!("Received MIDI_SYSEX_TYPE_NON_REALTIME ({MIDI_SYSEX_TYPE_NON_REALTIME:X})");

    let sysex_channel = read_one_byte(serial_port)?;
    trace!("Sysex channel is {:X}", sysex_channel);

    let sysex_general_information = read_one_byte(serial_port)?;
    if sysex_general_information != MIDI_SYSEX_GENERAL_INFORMATION {
        trace!(
            "Received {:x} but expected MIDI_SYSEX_GENERAL_INFORMATION ({:X})",
            sysex_general_information,
            MIDI_SYSEX_GENERAL_INFORMATION
        );
        return Ok(None);
    }

    trace!(
        "Received MIDI_SYSEX_GENERAL_INFORMATION ({MIDI_SYSEX_GENERAL_INFORMATION:X}): {:X?}",
        sysex_general_information
    );

    let sysex_reply_identity = read_one_byte(serial_port)?;
    if sysex_reply_identity != MIDI_SYSEX_REPLY_IDENTITY {
        trace!(
            "Received sysex reply of {:X}  but expected MIDI_SYSEX_REPLY_IDENTITY ({:X})",
            sysex_reply_identity,
            MIDI_SYSEX_REPLY_IDENTITY
        );
    }
    trace!(
        "Received MIDI_SYSEX_REPLY_IDENTITY: {:X?}",
        sysex_reply_identity
    );

    let manufacturer = read_one_byte(serial_port)?;
    let family_one = read_one_byte(serial_port)?;
    let family_two = read_one_byte(serial_port)?;
    let model_one = read_one_byte(serial_port)?;
    let model_two = read_one_byte(serial_port)?;
    let version_one = read_one_byte(serial_port)?;
    let version_two = read_one_byte(serial_port)?;
    let version_three = read_one_byte(serial_port)?;
    let version_four = read_one_byte(serial_port)?;

    let sysex_end = read_one_byte(serial_port)?;
    if sysex_end != MIDI_SYSEX_END {
        trace!(
            "Received sysex end of {:X}  but expected MIDI_SYSEX_END ({:X})",
            sysex_end,
            MIDI_SYSEX_END
        );
        return Ok(None);
    }

    let sysex_identity = SysexIdentity {
        manufacturer: u32::from_be_bytes([0, 0, 0, manufacturer]) as i64,
        family: u32::from_be_bytes([0, 0, family_one, family_two]) as i64,
        model: u32::from_be_bytes([0, 0, model_one, model_two]) as i64,
        version: format!(
            "{}.{}.{}.{}",
            version_one, version_two, version_three, version_four
        ),
    };
    debug!("Successful Sysex identity request. sysex_identity={sysex_identity:?}");

    Ok(Some(sysex_identity))
}

/// Perform a sysex identity request. Read here for more information on this:
/// http://midi.teragonaudio.com/tech/midispec/identity.htm
/// This method always returns a SysexIdentity struct, no matter if the device
/// has sysex identity support or not.
pub fn sysex_identity_request(
    serial_port: &mut Box<dyn SerialPort>,
    timeout: Duration,
) -> Result<Option<SysexIdentity>, Error> {
    let start = std::time::Instant::now();
    let mut last_sysex_send = std::time::Instant::now();
    debug!("Sending sysex request");
    serial_port.write_all(&[
        MIDI_SYSEX,
        MIDI_SYSEX_TYPE_NON_REALTIME,
        0x1,
        MIDI_SYSEX_GENERAL_INFORMATION,
        MIDI_SYSEX_REQUEST_IDENTITY,
        MIDI_SYSEX_END,
    ])?;

    loop {
        if start.elapsed() > timeout {
            warn!("Sysex request timed out.");
            break;
        }
        if serial_port.bytes_to_read()? > 0 {
            if let Ok(Some(sysex_identity)) = try_parsing_sysex_identity_reply(serial_port) {
                return Ok(Some(sysex_identity));
            }
        } else if last_sysex_send.elapsed() > Duration::from_millis(100) {
            debug!("Re-sending sysex request");
            serial_port.write_all(&[
                MIDI_SYSEX,
                MIDI_SYSEX_TYPE_NON_REALTIME,
                0x1,
                MIDI_SYSEX_GENERAL_INFORMATION,
                MIDI_SYSEX_REQUEST_IDENTITY,
                MIDI_SYSEX_END,
            ])?;
            last_sysex_send = std::time::Instant::now();
        }
    }

    Ok(None)
}
