use crate::error::Serial2MidiError;
use crate::sysex::sysex_identity_request;
use crate::sysex::SysexIdentity;
use core::fmt;
use rhai::{Engine, Scope};
use serialport::SerialPort;
use std::fmt::Display;
use std::time::Duration;
use tracing::debug;
use tracing::error;
use tracing::info;

#[derive(Debug)]
pub struct OurSerialPortInfo {
    pub port_name: String,
    pub usb_vid: i64,
    pub usb_pid: i64,
    pub usb_serial_number: String,
    pub usb_manufacturer: String,
    pub usb_product: String,
    pub sysex: SysexIdentity,
}

impl Default for OurSerialPortInfo {
    fn default() -> Self {
        Self {
            port_name: "".to_string(),
            usb_vid: -1,
            usb_pid: -1,
            usb_serial_number: "".to_string(),
            usb_manufacturer: "".to_string(),
            usb_product: "".to_string(),
            sysex: SysexIdentity::default(),
        }
    }
}

impl OurSerialPortInfo {
    pub fn from_serial_port(
        serial_port: &serialport::SerialPortInfo,
        sysex: SysexIdentity,
    ) -> Self {
        let mut usb_vid: i64 = -1;
        let mut usb_pid: i64 = -1;
        let mut usb_serial_number = "".to_string();
        let mut usb_manufacturer = "".to_string();
        let mut usb_product = "".to_string();
        if let serialport::SerialPortType::UsbPort(usb_port_info) = &serial_port.port_type {
            usb_vid = usb_port_info.vid as i64;
            usb_pid = usb_port_info.pid as i64;

            if let Some(serial_number) = &usb_port_info.serial_number {
                usb_serial_number = serial_number.clone()
            };
            if let Some(manufacturer) = &usb_port_info.manufacturer {
                usb_manufacturer = manufacturer.clone()
            };
            if let Some(product) = &usb_port_info.product {
                usb_product = product.clone()
            };
        }
        Self {
            port_name: serial_port.port_name.clone(),
            usb_vid,
            usb_pid,
            usb_serial_number,
            usb_manufacturer,
            usb_product,
            sysex,
        }
    }

    pub fn as_scope(&self) -> Scope {
        let mut scope = Scope::new();
        scope.push("sysex_manufacturer", self.sysex.manufacturer);
        scope.push("sysex_family", self.sysex.family);
        scope.push("sysex_model", self.sysex.model);
        scope.push("sysex_version", self.sysex.version.clone());
        scope.push("port_name", self.port_name.clone());
        scope.push("usb_vid", self.usb_vid);
        scope.push("usb_pid", self.usb_pid);
        scope.push("usb_serial_number", self.usb_serial_number.clone());
        scope.push("usb_product", self.usb_product.clone());

        scope
    }
}
impl Display for OurSerialPortInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "port_name: \"{}\"", self.port_name)?;
        writeln!(f, "usb_vid: {:#06x}", self.usb_vid)?;
        writeln!(f, "usb_pid: {:#06x}", self.usb_pid)?;
        writeln!(f, "usb_serial_number: \"{}\"", self.usb_serial_number)?;
        writeln!(f, "usb_manufacturer: \"{}\"", self.usb_manufacturer)?;
        writeln!(f, "usb_product: \"{}\"", self.usb_product)?;
        writeln!(f, "sysex_manufacturer: {:#06X}", self.sysex.manufacturer)?;
        writeln!(f, "sysex_family: {:#06x}", self.sysex.family)?;
        writeln!(f, "sysex_model: {:#06x}", self.sysex.model)?;
        write!(f, "sysex_version: \"{}\"", self.sysex.version)
    }
}

const RANDOM_SLEEP: [Duration; 5] = [
    Duration::from_millis(100),
    Duration::from_millis(200),
    Duration::from_millis(300),
    Duration::from_millis(400),
    Duration::from_millis(500),
];

/// Randomly sleep for 1, 0.5 or 2 seconds
pub fn random_sleep() {
    use rand::Rng;
    let mut rng = rand::thread_rng();

    let random_index: usize = rng.gen_range(0..RANDOM_SLEEP.len());
    std::thread::sleep(RANDOM_SLEEP[random_index]);
}

pub fn find_matching_serial_port(
    engine: &mut Engine,
    match_expression: &str,
    timeout: Duration,
    baud_rate: u32,
) -> Result<Option<(Box<dyn SerialPort>, OurSerialPortInfo)>, Serial2MidiError> {
    let mut found_serial_port = None;
    for port_info in serialport::available_ports()?.into_iter() {
        println!("Probing {}", port_info.port_name);
        let serial_port = serialport::new(port_info.port_name.clone(), baud_rate)
            .timeout(timeout)
            .open();

        if let Err(err) = serial_port {
            error!("Error trying to open serial port: {}", err);
            random_sleep();
            continue;
        }

        let mut serial_port = serial_port?;

        let sysex = match sysex_identity_request(&mut serial_port, timeout) {
            Ok(Some(sysex)) => sysex,
            Ok(None) => {
                error!("No sysex for device");

                SysexIdentity::default()
            }
            Err(err) => {
                error!("Error on try_sysex_request: {}", err);

                SysexIdentity::default()
            }
        };

        let our_serial_port_info = OurSerialPortInfo::from_serial_port(&port_info, sysex);
        let mut scope = our_serial_port_info.as_scope();

        debug!("\n# Device\n{}", our_serial_port_info);
        debug!("Match Expression: {}", match_expression);

        let result: bool = engine
            .eval_expression_with_scope(&mut scope, match_expression)
            .map_err(|err| Serial2MidiError::RhaiExpressionError(err.to_string()))?;
        info!("Match: {}", result);
        if result {
            drop(serial_port);
            // Open serial port again so that we have a fresh connection (sysex parser might drop
            // important packages)
            let serial_port = serialport::new(port_info.port_name.clone(), baud_rate)
                .timeout(timeout)
                .open();

            let serial_port = match serial_port {
                Ok(serial_port) => serial_port,
                Err(err) => {
                    error!(
                        "Error trying to open serial port, but weirdly the first time it worked?: {}",
                        err
                    );
                    random_sleep();
                    continue;
                }
            };

            found_serial_port = Some((serial_port, our_serial_port_info));
            break;
        }
    }

    Ok(found_serial_port)
}

pub fn list_devices(
    timeout: Duration,
    baud_rate: u32,
) -> Result<Vec<OurSerialPortInfo>, Serial2MidiError> {
    Ok(serialport::available_ports()?
        .into_iter()
        .filter_map(|port_info| {
            debug!("Opening {}", port_info.port_name);
            let serial_port = serialport::new(port_info.port_name.clone(), baud_rate)
                .timeout(timeout)
                .open();
            let mut serial_port = match serial_port {
                Ok(serial_port) => serial_port,
                Err(err) => {
                    error!("Error when trying to open serial port: {}", err);
                    random_sleep();
                    return None;
                }
            };

            let sysex = match sysex_identity_request(&mut serial_port, timeout) {
                Ok(Some(sysex)) => sysex,
                Ok(None) => {
                    error!("No sysex for device");
                    SysexIdentity::default()
                }
                Err(err) => {
                    error!("Error on try_sysex_request: {}", err);
                    SysexIdentity::default()
                }
            };

            Some(OurSerialPortInfo::from_serial_port(&port_info, sysex))
        })
        .collect())
}
