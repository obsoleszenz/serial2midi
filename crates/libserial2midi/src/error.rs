use thiserror::Error;

#[derive(Error, Debug)]
pub enum Serial2MidiError {
    #[error("Serialport error")]
    SerialportError(#[from] serialport::Error),
    #[error("Error in expression")]
    RhaiExpressionError(String),
    #[error("Midir Connect error")]
    MidirConnectError(#[from] midir::ConnectError<()>),
    #[error("Midir Init error")]
    MidirInitError(#[from] midir::InitError),
}
