use std::{thread::sleep, time::Duration};

use midir::os::unix::{VirtualInput, VirtualOutput};
use tracing::{debug, error, trace};

use crate::{
    error::Serial2MidiError,
    serial::{start_serial_midi_finder, Message},
};

/// This method creates a virtual midi device, starts the thread
/// that finds, opens and reconnects to the matching serial port
/// and connects those two things to bridge midi messages to each other.
/// This method does not return, besides of an unrecoverable error.
pub fn start_virtual_midi(
    name: &str,
    match_expression: &str,
    baud_rate: u32,
    latency: Duration,
    timeout: Duration,
) -> Result<(), Serial2MidiError> {
    let (mut midi_input_tx, mut midi_output_rx) =
        start_serial_midi_finder(match_expression.to_string(), baud_rate, latency, timeout);

    debug!("Starting virtual alsa midi in port");

    // Open the midi input port with the callback
    // that pushes midi messages from the serial port
    // to the virtual midi device
    let _port_midir_input = midir::MidiInput::new(name)?
        .create_virtual(
            name,
            move |_timestamp, bytes, _user_data| {
                debug!("[-->] {:X?}", bytes);
                let bytes: [u8; 3] = [bytes[0], bytes[1], bytes[2]];
                if let Err(rtrb::PushError::Full(_)) = midi_input_tx.push(Message::SendMidi(bytes)) {
                    trace!("midi_input_tx ringbuffer is full. This should be fine, most likely we don't have an open serial connection");
                }
            },
            (),
        )
        // map_err to circumvent https://github.com/Boddlnagg/midir/issues/55
        .map_err(|e| midir::ConnectError::new(e.kind(), ()))?;

    // Create the virtual midi output port
    let mut port_midir_output = midir::MidiOutput::new(name)?
        .create_virtual(name)
        // map_err to circumvent https://github.com/Boddlnagg/midir/issues/55
        .map_err(|e| midir::ConnectError::new(e.kind(), ()))?;

    // Loop that receives the midi messages from the serial port that should
    // get send to the midi output port
    loop {
        while let Ok(Message::ReceivedMidi(message)) = midi_output_rx.pop() {
            debug!("[<--] {:X?}", message);
            if let Err(err) = port_midir_output.send(&message) {
                error!("Error sending to port_midir_output: {}", err);
            }
        }
        sleep(latency);
    }
}
