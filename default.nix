
{ pkgs ? import <nixpkgs> { } }:
let manifest = (pkgs.lib.importTOML ./crates/cli/Cargo.toml).package;
in
pkgs.rustPlatform.buildRustPackage rec {
  pname = manifest.name;
  version = manifest.version;
  cargoLock.lockFile = ./Cargo.lock;
  src = pkgs.lib.cleanSource ./.;
  nativeBuildInputs = with pkgs; [
    pkg-config    
  ];
  buildInputs = with pkgs; [
    alsa-lib.dev
    clang
    libudev-zero
    pkg-config    
  ];
}
