<div align="right">
   <a href="(https://www.gnu.org/licenses/gpl-3.0"><img src="https://codeberg.org/obsoleszenz/serial2midi/raw/commit/0b048c96acb66efdc6626be06faae81b54a17791/assets/license-badge.png" alt="logo" width="120px"></a>
</div>
<br>
<br>
<div align="center">
  <img src="https://codeberg.org/obsoleszenz/serial2midi/raw/commit/211e87fe692ac7827d9a69b055e0daf71b903771/assets/icon.png" alt="logo" width="200">
  <br>
  <h1>serial2midi</h1>

</div>
  <h4 align="center">Turn any serial port into a virtual midi device</h4>

  <br>




# What is this?

[serial2midi](https://codeberg.org/obsoleszenzia/serial2midi) is a tool that allows
to create virtual [MIDI](https://wikipedia.org/wiki/midi) devices out of serial 
devices. This can be very useful if one is building a MIDI controller with Arduinos
 which don't support USB MIDI. This tool registers a virtual midi device on the ALSA
layer, which means that one can also use it from JACK/PipeWire.

# Features

- Duplex (Sending & Receiving MIDI)
- Sysex Identity to differentiate midi devices with the same usb vid/pid/serial number...
- Unplug protection/reconnection
- Low latency

# Requirements
- Linux/Alsa

Virtual MIDI devices will be usable also from JACK/PipeWire (for JACK you might need a2jamidi).

# Mac OS/Windows Support?
If you are using Windows or Mac, please have a look at [Hairless MIDI](https://projectgus.github.io/hairless-midiserial/).
While this tool might work on Windows or Mac, I neither tested it on them nor will I provide any support.

# Installation

1. Make sure you have rust/rustup installed and you use latest stable
2. Clone this repository
3. Cd into the `serial2midi` folder
4. Execute `cargo install --path cli`
5. You should now be able to run the executable from your terminal with `serial2midi`

# Usage

## Listing available devices

Simply execute `serial2midi list` to list all available serial devices.
Example output:
```
port_name: "/dev/ttyACM0"
usb_vid: 0x2341
usb_pid: 0x0043
usb_serial_number: "75335313437351414212"
usb_manufacturer: "Arduino SA"
usb_product: "Uno R3 (CDC ACM)"
sysex_manufacturer: 0x006F
sysex_family: 0x0200
sysex_model: 0x0000
sysex_version: "1.0.0.0"

port_name: "/dev/ttyUSB0"
usb_vid: 0x1a86
usb_pid: 0x7523
usb_serial_number: ""
usb_manufacturer: "QinHeng Electronics"
usb_product: "CH340 serial converter"
sysex_manufacturer: 0x006F
sysex_family: 0x0000
sysex_model: 0x0000
sysex_version: "1.0.0.0"

port_name: "/dev/ttyUSB1"
usb_vid: 0x1a86
usb_pid: 0x7523
usb_serial_number: ""
usb_manufacturer: "QinHeng Electronics"
usb_product: "CH340 serial converter"
sysex_manufacturer: 0x006F
sysex_family: 0x0100
sysex_model: 0x0000
sysex_version: "1.0.0.0"
```

## Midify a serial device

First we need to decide which serial device we want to midify. In this example we will want to midify the serial device with the usb_pid 4342.
And we want our virtual midi device to have the name `midified`.
The full command would be:
```
serial2midi midify --name "midified" --match 'port_name == "/dev/ttyUSB1"'
```

You can also do more complicated match epxression like this

```
serial2midi midify --name "midified" --match 'port_name == "/dev/ttyUSB1" && usb_pid == 4242'
```

Following variables are supported (all returned by the list command):

- port_name: String
- usb_vid: Number
- usb_pid: Number
- usb_serial_number: String
- usb_manufacturer: String
- usb_product: String
- sysex_manufacturer: Number
- sysex_family: Number
- sysex_model: Number
- sysex_version: String

The match expressions are a powerful feature as you can write expression that you might know from programming. to find the serial device you want to midify.
You are also allowed to use logic operators and a lot more. We embed the  [rhai](https://rhai.rs) scripting language for this features.
Check out the [rhai documentation](https://rhai.rs/book/appendix/operators.html) for more details.

# Sysex Identity

Sysex Identity is a MIDI sysex protocol that provides additional information about a MIDI device. This works by sending a special sysex mesage called the sysex
identity request to the midi device and the midi device, in case it support this, replies with the sysex identity reply. For more information about the protocol,
have a look here: http://midi.teragonaudio.com/tech/midispec/identity.htm

# Latency

The latency can be configured with the `--latency` argument. It controls how long `serial2midi` will wait to bridge more incoming/outgoing messages. The default is set to 15ms.
The lower you set the latency, the higher will be the cpu usage & the quicker we will react to midi events.

# Baud Rate

The Baud rate is an important setting for serial devices. You can set it with the `--baud-rate` flag and it defaults to 115200. Read more about it on [wikipedia](https://en.wikipedia.org/wiki/Baud).

# Shell Completions

If you'd like to make use of the built-in shell completion support, you need to run `serial2midi print-completions <your-shell>` and put the completions in the correct place 
for your shell. A few examples with common paths are provided below:

```
# For bash
serial2midi print-completions bash > ~/.local/share/bash-completion/completions/miniserve

# For zsh
serial2midi print-completions zsh > /usr/local/share/zsh/site-functions/_miniserve

# For fish
serial2midi print-completions fish > ~/.config/fish/completions/miniserve.fish
```


# Debugging

To get more insight into what serial2midi is doing, enable debug logs by setting the RUST_LOG environment variable to debug.
Example: `RUST_LOG=debug serial2midi list`

# Questions?

Feel free to ask questions by simply opening a new issue :)

# ToDo

- More live testing
- Sysex identity parsing is suboptimal
- Midi parsing in general is suboptimal

# Similiar projects

## Serial to virtual MIDI bridges
- [Hairless MIDI](https://projectgus.github.io/hairless-midiserial/)

## Alternative Arduino USB Firmware
- [mocoLUFA](https://github.com/kuwatay/mocolufa)
- [hiduino](https://github.com/ddiakopoulos/hiduino)

