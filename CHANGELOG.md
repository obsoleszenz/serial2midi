# CHANGELOG

## Unreleased

- Moved crates into own subfolder (./crates)
- Finding tty device should now be faster
    - Rewrote the sysex request
- Better parsing of midi bytes to not read invalid midi messages from tty
- Added --timeout flag to cli
- Updated dependencies

## v1.0.0 -- 27.09.2024


- First tagged release!
- Updated nix files
- Bumped dependencies 
